

<section id="from-the-blog" class="shadow-holder">
<div class="container">
      <div class="row">
        <div class="col-sm-10 col-xs-12">
          <h2>From The Blog</h2>
                <span class="shadow">From The Blog</span>          </div>
        <div class="col-sm-2 col-xs-12 review-arrows">
        <a class="left" href="#myCarousel2" data-slide="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
        <a class="right" href="#myCarousel2" data-slide="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a> 
        
        </div>
    </div>
    
  <div id="myCarousel2" class="carousel slide" data-ride="carousel">

 <div class="carousel-inner">
 
 <?php
$args = array(
      'numberposts'   => '9',
      'post_status'   => 'publish',
    );
	
$recent_posts = wp_get_recent_posts( $args );
$countposts = 0;

	foreach( $recent_posts as $recent ) {
	$countposts++;
	
	if(get_the_title($recent["ID"])){$title = get_the_title($recent["ID"]); }else{ unset($title); }
	if(get_the_post_thumbnail($recent["ID"])){ $image = get_the_post_thumbnail($recent["ID"]); }else{ unset($image); }
	if(md5_file('https://www.wealthsafe.com.au/wp-content/uploads/2010/09/thumbnail-150.png') === md5_file($image)){ $imagechk = true; }else{ $imagechk = false; }
	if(!$image || $imagechk){ $image = get_site_url().'/wp-content/uploads/2014/06/header3.png'; }
	if(get_cat_name($recent["ID"])){ $cat = get_cat_name($recent["ID"]); }else{ unset($cat); }
	if(get_post_permalink($recent["ID"])){ $link = get_post_permalink($recent["ID"]); }else{ unset($link); }
	if(get_post_meta($recent["ID"], '_yoast_wpseo_metadesc', true)){ $desc = substr(get_post_meta($recent["ID"], '_yoast_wpseo_metadesc', true), 0, 125).'...'; }else{ $desc = substr(strip_tags($recent["post_content"]), 0, 125).'...'; }
	$comments_count = wp_count_comments( $recent["ID"] );
	 
		if($countposts == '1' || $countposts == '4' || $countposts == '7' || $countposts == '10'){ 
		echo '<div class="item';
		if($countposts == '1'){ echo ' active'; }
		echo '"><div class="row">';
		} 
	  	
					echo '<div class="col-sm-4 col-xs-12">';
					if($image){ echo '<a href="'.$link.'">'.$image.'</a>'; }
					
					
					echo '<div class="blog-content"><p class="cat">'.$cat.'</p><h3><a href="'.$link.'">'.$title.'</a></h3>';
					echo '<p>'.$desc.'</p>';
					
					echo '<div class="stats">';
         
                   echo '<div class="stat">';
                  echo '<i class="fa fa-comments" aria-hidden="true"></i>'.$comments_count->total_comments.'</div>';
            
                    echo '</div>';
                    echo '<div style="clear:both;"></div>';
					
					echo '</div>';
					echo '</div>';
		
		if($countposts == '3' || $countposts == '6' || $countposts == '9'|| $countposts == '12'){ 
		echo '</div></div>';
		}
		
	}

wp_reset_query();
?>
     
     
     
      </div>
    </div>
    </div>
</section>