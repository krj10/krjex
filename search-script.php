<?php
?>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<?php
			$page = isset($_GET['page']) ? $_GET['page'] : '';	
			require_once("app/controllers/routes.php");

			require_once("".structure."head.php"); 

		?>	
		<title> Impresso Brasil  | <?php echo ucwords($page) ?> </title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper-->
		
<?php require_once("".structure."header.php"); ?>

<main class="main" style="padding-bottom:80px;">
		<section class="header-page">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 hidden-xs">
						<h1 class="mh-title">Busca</h1>
					</div>
					<div class="breadcrumb-w col-sm-8">
						<span class="hidden-xs">Você esta aqui:</span>
						<ul class="breadcrumb">
							<li>
								<a href="<?php echo HOST ?>">Home</a>
							</li>
							<li>
								<span>Busca</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="or-service">
	<div class="container">
		<div class="row">
			<div class="block-title-w">
			</div>
			<div class="or-service-w">
                
                
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
            <form action="<?php echo HOST.'busca.php'; ?>" style="margin-bottom:35px;">
            <input type="text" name="busca" style="padding:6px;border:1px solid #cccccc;border-radius:5px;font-size:15px;width:70%;" placeholder="Busca..." required/>
            <button style="background:#6eb74e;color:#ffffff;font-size:16px;padding:5px 15px;;margin-left:15px;border-radius:5px;"><i class="fa fa-search"></i></button>
             </form>
                    </div></div>
                
                
                
                
                
            <?php
                
           // echo $_GET['busca'];
                
          /*  $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $getv = explode('=', $actual_link);
            if($getv[1]){*/
                
                $normalizeChars = array(
                            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
                            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
                            'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
                            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
                            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
                            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
                            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
                            'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
                        );

            
                $busc = $_GET['busca'];
                $busc = strtr($busc, $normalizeChars);
          
                                   
            $searchforv = str_replace(",", " ", $busc); // accepts both space and comma as separator
            $results = explode(" ", $searchforv); // put all words in a array
            $results = array_map('strtolower', $results); // put the entire array in lower case to avoid matching problems
            foreach($results as $key => $value ){ if($value == 'de' || $value == 'a' || $value == 'e'  || $value == 'para'  || $value == 'no'){ unset($results[$key]); } } // get rid of prepositions for better results
           
    
            
              $getS = $conn->prepare("SELECT * FROM produtos");
              $getS->execute();
              foreach($getS->fetchAll() as $s){
            
                  unset($nomes);
                  $nomes = array();
                  
                  $getname = $s['nome'];
                  $getname = strtr($getname, $normalizeChars);
                  
                  $limpanomes = str_replace(",", " ", $getname); // accepts both space and comma as separator
                  $limpanomes = str_replace("-", " ", $limpanomes); 
                    $nomes = explode(" ", $limpanomes); // put all words in a array
                    $nomes = array_map('strtolower', $nomes); // put the entire array in lower case to avoid matching problems
                   foreach($nomes as $key => $value ){ if($value == 'de' || $value == 'a' || $value == 'e'  || $value == 'para'  || $value == 'no'){ unset($nomes[$key]); } } // get rid of prepositions for better results
                  
               
                  
                  
                   $diff = array_intersect($results, $nomes); 
                  
                  if($diff == true){
                  $found = true;
                  ////
                  
                  
                        $id = $s['id'];
                        $toalias = $s['nome'];
                        $str = str_replace('-', ' ', $toalias);
                        $str = str_replace('_', ' ', $toalias);
                  
                  
                      

                        $str = strtr($str, $normalizeChars);

                        // remove any duplicate whitespace, and ensure all characters are alphanumeric
                        $str = preg_replace(array('/\s+/','/[^A-Za-z0-9\-]/'), array('-',''), $str);

                        // lowercase and trim
                        $str = trim(strtolower($str));
                        // return $str;
                  
                      

                        $alias = $str;

                  
                  
                  $link = HOST.'produto/'.$alias.'/'.$id.'/';
            ?>
                
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6 or-block">
					<div style="text-align:left;">
						<a href="<?php echo $link; ?>">
							<img src="<?php echo HOST; ?>app/content/images/produto/<?php echo $s['foto']; ?>" alt="<?php echo $s['nome']; ?>" style="height:175px;width:auto;max-width:100%;display:inline-block;box-shadow: 4px 4px 10px #888888;" />
						</a>
					</div>
				</div>
                <div class="col-md-3 col-sm-6 col-xs-6 or-block">
                    <div class="or-title" style="display:block;text-align:left;margin:0 0 25px;font-size:16px;">
						<a href="<?php echo $link; ?>"><?php echo strtoupper($s['nome']); ?></a>
					</div>
                    <div style="margin-bottom:25px;text-align:left;color:#666666;"><?php echo substr($s['descricao'], 0, 100); ?></div>
                    <p style="text-align:left;"><a href="<?php echo $link; ?>" class="btn" style="padding:5px 15px;background:#6eb74e;color:#ffffff;font-weight:bold;border-radius:7px;">SAIBA +</a></p>
                </div>
            </div>
                
            <?php }}
                //}
                
                
            if(!$found){echo '<p>Infelizmente não foi encontrado nenhum resultado para esta busca.</p>';}
                
                ?>
                
				
			</div>
		</div>
	</div>
</section> 
        </main>

    		
<?php require_once("".structure."footer.php"); ?>
    
        </body>
</html>

